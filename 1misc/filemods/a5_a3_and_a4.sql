﻿select oldpath, max(linecnt) as lines, count(*) as changes, sum(replacecnt)
from filemods
where modtype like 'modify'
group by oldpath
order by changes desc;