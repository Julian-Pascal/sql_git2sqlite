﻿--drop table _change_dist;
--create table _change_dist as
SELECT oldpath, MAX(linecnt) AS LINES, COUNT(*) AS cnt_changes, SUM(replacecnt) AS sum_changes, (COUNT(*) * SUM(replacecnt)) AS cntxsum
FROM filemods
WHERE modtype LIKE 'modify'
GROUP BY oldpath
ORDER BY cntxsum DESC;