import sqlite3
from sqlite3.dbapi2 import Connection
import os

REPO_NAMES = ['anki',
              'hugsql',
              'git2pg',
              'arsnova.click',
              'cards',
              'h2database',
              'jgit',
              'bootstrap',
              'atom',
              'git']


def get_conn(name: str) -> Connection:
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, '..', 'sqlite-files', name + '_mirror.sqlite3')
    filename_abs = os.path.abspath(filename)
    print(os.path.abspath(filename_abs))
    return sqlite3.connect(filename_abs)
