drop table if exists _Branch_master;
--create table _Master (commitid varchar(40), branch varchar(255), oldpath text);
create table _Branch_master as select * from Filemods join Branches using (commitid) where branch like '%master';

select oldpath, max(linecnt) as lines, count(*) as cnt_changes, sum(replacecnt) as sum_changes, (count(*) * sum(replacecnt)) as cntxsum
from _Branch_master
where modtype like 'modify'
group by oldpath
order by cntxsum desc;