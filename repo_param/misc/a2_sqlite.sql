﻿drop table _change_dist;
create table _change_dist as
select oldpath, max(linecnt) as lines, count(*) as cnt_changes, sum(replacecnt) as sum_changes, (count(*) * sum(replacecnt)) as cntxsum
from filemods
where modtype like 'modify'
group by oldpath
order by cntxsum desc;

--drop table _repo_param;
create table _repo_param (
    name varchar(255) primary key,
    usercount int,
    filecount int,
    commitcount int,
    lifetime interval
);

insert into _repo_param
    values ('cards',
        (select count(id) from users),
        (select count(sub.oldpath)
          from (select oldpath
            from filemods
            union
            select newpath
            from filemods) sub),
        (select count(commitid) from commits),
        (select max(committerts) - min(committerts) from commits));