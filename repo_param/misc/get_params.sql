select *
from (select count(id) as 'User' from Users),
     (select count(distinct author) as Authors from Commits),
     (select count(distinct committer) as Committers from Commits),
     (select count(distinct Filemods.newpath) as 'Dateien' from Filemods),
     (select count(distinct commitid) as 'Commits (Master)' from Branches where branch like '%master'),
     (select count(distinct commitid) as 'Commits (alle Branches)' from Commits),
     (select max(committerts) - min(committerts) as 'Lebensdauer' from Commits);