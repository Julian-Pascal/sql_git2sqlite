insert into _repo_param
    values ('cards',
        (select count(id) from users),
        (select count(sub.oldpath)
          from (select oldpath
            from filemods
            union
            select newpath
            from filemods) sub),
        (select count(commitid) from commits),
        (select max(committerts) - min(committerts) from commits));