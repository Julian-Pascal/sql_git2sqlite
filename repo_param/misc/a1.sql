--create table _usercount as
select count(id) from users;

--create table _filecount as
select count(path)
from(
    select oldpath
    from filemods
    union
    select newpath
    from filemods
) as path;

--create table _commitcount as
select count(commitid) from commits;

select max(committerts)
from commits
union all
    select min(committerts)
    from commits;

--create table _lifetime as
select age((select max(committerts) from commits), (select min(committerts) from commits));