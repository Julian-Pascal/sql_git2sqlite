create table _repo_param (
    name varchar(255) primary key,
    usercount int,
    filecount int,
    commitcount int,
    lifetime interval
);