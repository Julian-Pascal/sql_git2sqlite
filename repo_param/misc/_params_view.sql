drop view params;
create view params as
  select count(Users.id),
         count(distinct Filemods.newpath),
         count(Commits.commitid),
         (max(committerts) - min(committerts))/1000
  from Users, Commits, Filemods