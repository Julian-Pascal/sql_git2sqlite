import pandas as pd

from a_lib.repo_db import get_conn, REPO_NAMES

GET_PARAMS = """
select *
from (select count(id) as 'User' from Users),
     (select count(distinct author) as Authors from Commits),
     (select count(distinct committer) as Committers from Commits),
     (select count(distinct Filemods.newpath) as 'Dateien' from Filemods),
     (select sum(Filemods.insertcnt - Filemods.deletecnt) as 'Zeilen (Master)' 
        from Filemods join Branches 
        using (commitid) 
        where branch like 'refs/heads/master'),
     (select count(distinct commitid) as 'Commits (Master)' from Branches where branch like 'refs/heads/master'),
     (select count(distinct commitid) as 'Commits' from Commits),
     (select max(committerts) - min(committerts) as 'Lebensdauer' from Commits)
"""


def write_params_to_latex():
    param_list = []
    for name in REPO_NAMES:
        df = pd.read_sql(GET_PARAMS, get_conn(name))
        df['Name'] = name
        df.set_index('Name', inplace=True)
        param_list.append(df)
    dfg: pd.DataFrame = pd.concat(param_list)
    dfg['Lebensdauer'] = pd.to_timedelta(dfg['Lebensdauer'], unit='ms')
    dfg.sort_values(by=['Commits (Master)'], inplace=True)

    # print(tb(dfg))
    with open('repo_params.tex', 'w') as f:
        f.write(dfg.to_latex())
    return dfg
