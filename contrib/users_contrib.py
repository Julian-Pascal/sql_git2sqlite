import sqlite3
import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame
import seaborn as sns; sns.set()

from a_lib.repo_db import get_conn, REPO_NAMES

"""
select name, email, sum(insertcnt + deletecnt + replacecnt) as contribution
from Commits as t
    join Users U on t.author = U.id
    join Filemods using (commitid)
group by author
order by contribution desc;
"""

SQL_CONTRIB = """select name, email, sum(insertcnt + deletecnt + replacecnt) as contribution
from Commits as C
         join Branches B on C.commitid = B.commitid
         join Users U on C.author = U.id
         join Filemods using (commitid)
where B.branch like 'refs/heads/master'
group by U.name
order by contribution desc;
"""


def users_contrib_pie(name):
    conn = get_conn(name)
    df = pd.read_sql(SQL_CONTRIB, conn)
    #ax = df.plot(kind='pie', y='contribution', index=['1'])
    ax = df.plot.pie(y='contribution', use_index=False)
    #ax.legend(df['name'])
    ax.get_legend().remove()
    plt.show()


def users_contrib_list(name):
    conn = get_conn(name)
    df = pd.read_sql(SQL_CONTRIB, conn)
    df['p'] = df['contribution'] / df['contribution'].sum()
    return df


def do_all():
    for name in REPO_NAMES:
        do_it(name)


def do_it(name):
    df: DataFrame = users_contrib_list(name)
    # df.replace
    # print(df)
    df.to_csv(f'./users_contrib_out/{name}.csv')
    with open(f'./users_contrib_out/{name}-tab.tex', 'w') as f:
        f.write(df.truncate(after=5).to_latex())
    return df


def do_aclick():
    name = 'arsnova.click'
    df: DataFrame = users_contrib_list(name)
    ndf: DataFrame = df.replace({'name': {'cmfl37': 'Christopher Fullarton'}})\
        .replace({'name': {'Christopher Mark Fullarton': 'Christopher Fullarton'}})\
        .groupby('name')\
        .sum()\
        .sort_values(by=['contribution'], ascending=False)\
        .reset_index()
    print(df.truncate(after=5))
    print(ndf)
    ndf.to_csv(f'./users_contrib_out/{name}.csv')
    with open(f'./users_contrib_out/{name}-tab2.tex', 'w') as f:
        f.write(ndf.truncate(after=5).to_latex())
    return ndf


def do_cards():
    name = 'cards'
    df: DataFrame = users_contrib_list(name)
    ndf = df.replace({'name': {'Klaus-Dieter Quibeldey-Cirkel': 'Klaus Quibeldey-Cirkel'}})\
        .groupby('name')\
        .sum()\
        .sort_values(by=['contribution'], ascending=False)\
        .reset_index()
    print(df.truncate(after=5))
    print(ndf)
    ndf.to_csv(f'./users_contrib_out/{name}.csv')
    with open(f'./users_contrib_out/{name}-tab1.tex', 'w') as f:
        f.write(ndf.truncate(after=5).to_latex())
    return ndf


def do_anki():
    do_it('anki')


def do_h2():
    name = 'h2database'
    df: DataFrame = users_contrib_list(name)
    ndf = df.replace({'name': {'noelgrandin': 'Noel Grandin'}}) \
        .groupby('name') \
        .sum() \
        .sort_values(by=['contribution'], ascending=False) \
        .reset_index()
    print(df.truncate(after=5))
    print(ndf)
    ndf.to_csv(f'./users_contrib_out/{name}.csv')
    with open(f'./users_contrib_out/{name}-tab1.tex', 'w') as f:
        f.write(ndf.truncate(after=5).to_latex())
    return ndf


def new_do_all():
    do_all()
    do_aclick()
    do_cards()
    do_anki()

#for name in REPO_NAMES:
#    users_contrib(name)
# do_all()
