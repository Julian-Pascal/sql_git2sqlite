import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

from a_lib.repo_db import get_conn, REPO_NAMES

CONTRIB_TIME = """
select round(committerts/(86400000*30)) as t, printf('%s  %s', name, email) as id, sum(insertcnt + deletecnt + replacecnt) as contribution
from Commits as t
    join Users U on t.author = U.id
    join Filemods using (commitid)
group by author, round(committerts/(86400000*30))
order by t ,contribution desc;
"""


def contrib_time(name, rel):
    df = pd.read_sql(CONTRIB_TIME, get_conn(name))
    df = df.pivot(index='t', columns='id', values='contribution')
    if rel:
        df = df.divide(df.sum(axis=1), axis=0)

    print(df)
    df.plot.area(legend=False)
    plt.show()


contrib_time('git', False)
