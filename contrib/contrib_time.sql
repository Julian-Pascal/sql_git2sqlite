select round(committerts/(86400000*30)) as t, author
from Commits join Filemods using (commitid)
group by round(committerts/(86400000*30)), author;

select committerts, author
from Commits join Filemods using (commitid);

select round(committerts/(86400000*30)) as t, name, sum(insertcnt + deletecnt + replacecnt) as contribution
from Commits as t
    join Users U on t.committer = U.id
    join Filemods using (commitid)
group by committer, round(committerts/(86400000*30))
order by contribution desc;