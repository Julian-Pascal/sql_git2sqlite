## Zur Einrichtung

- Die nötigen Abhängigkeiten könnnen über ```pip3 install -r requirements.txt``` installiert werden.

- Um die Auswertungen, die als Jupyter-Notebooks vorhanden sind zu benutzen, wird PyCharm Professional benötigt.
Diese müssen darin als „Managed Jupyter Server“ gestartet werden.