from itertools import combinations
from typing import Iterable
import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame
import seaborn as sns
# from matplotlib.ticker import PercentFormatter

from a_lib.repo_db import REPO_NAMES
from a_lib.repo_db import get_conn
from architecture.change_coupling_files.view import *
from architecture.change_coupling_files.sql_templates import *


def write_latex(name, df: DataFrame):
    with pd.option_context("max_colwidth", 1000):
        with open(f'output/{name}.tex', 'w') as f:
            f.write(df.to_latex())


def update_views_and_tables(name):
    conn = get_conn(name)
    create_change_coupling(conn)

def delete_all():
    for n in REPO_NAMES:
        conn = get_conn(n)
        delete_change_coupling(conn)

def update_all():
    for n in REPO_NAMES:
        update_views_and_tables(n)


def get_dataframe(name, sql, module=None) -> DataFrame:
    conn = get_conn(name)
    return pd.read_sql(sql, conn, params=module)


def cc_heat(name):
    get_sql = """select *
    from _change_coupling
    where lf < rf;
    """
    conn = get_conn(name)
    create_change_coupling(conn)
    df = pd.read_sql(get_sql, conn)
    tab = pd.crosstab(df.lf, columns=df.rf)
    return tab
    plt.figure(figsize=(20,15))
    ax = sns.heatmap(tab)
    plt.show()


def cc_pair(name: str, a: str, b: str) -> DataFrame:
    coupling_sql = """
    select count(distinct commitid) as n from _change_coupling
    where lf like ? and rf like ?;
    """
    all_sql = """
    select count(distinct commitid) as n from _change_coupling
    where lf like ?;
    """
    conn = get_conn(name)
    coupled: DataFrame = pd.read_sql(coupling_sql, conn, params=(a, b)).at[0, 'n']
    occurrences: DataFrame = pd.read_sql(all_sql, conn, params=([a])).at[0, 'n']
    print(f"{coupled}/{occurrences}")
    return coupled/occurrences


def cc_multi(terms: Iterable[str]):
    for a, b in combinations(terms, 2):
        print(a, b)
        res = cc_pair('git2pg', a, b)
        print(res)
    return "TODO"


def cc_file(name, lf):
    # update_views_and_tables(name)
    return get_dataframe(name, sql_1toN_single(), [lf] * 2)


def cc_file_before(name, lf):
    return get_dataframe(name, sql_1toN_single('_change_coupling_before'), [lf] * 2)


def cc_file_after(name, lf):
    return get_dataframe(name, sql_1toN_single('_change_coupling_after'), [lf] * 2)
