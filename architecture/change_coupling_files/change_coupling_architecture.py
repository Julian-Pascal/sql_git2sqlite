import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame
import seaborn as sns
from matplotlib.ticker import PercentFormatter

from a_lib.repo_db import REPO_NAMES
from a_lib.repo_db import get_conn
from architecture.change_coupling_files.view import create_change_coupling
from architecture.change_coupling_files.sql_templates import *


def change_coupling_test_prod(name: str) -> float:
    test_prod_sql = """
    select count(distinct commitid) from _change_coupling
    where (lf like '%test%' and rf not like '%test%') or (lf not like '%test%' and rf like '%test%');
    """
    conn = get_conn(name)
    coupled = pd.read_sql(test_prod_sql, conn)
    occurrences = pd.read_sql('select count(distinct commitid) from _change_coupling', conn)
    return (coupled/occurrences).set_index([[name]])


def plot_change_coupling_test_prod():
    for name in REPO_NAMES:
        create_change_coupling(get_conn(name))

    m = map(change_coupling_test_prod, REPO_NAMES)
    print(type(m))

    df: DataFrame = pd.concat(m)
    print(df)
    df.plot.barh()
    plt.gca().xaxis.set_major_formatter(PercentFormatter(1))
    plt.show()
    plt.close()
