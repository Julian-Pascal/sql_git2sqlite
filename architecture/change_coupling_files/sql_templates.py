SQL_1toN_ALL = """
SELECT lf, count(commitid) AS n 
FROM _change_coupling
WHERE lf != rf
GROUP BY lf
ORDER BY n DESC;
"""
SQL_1toN_FILTER = """
SELECT lf, count(commitid) AS n 
FROM _change_coupling
WHERE lf != rf
AND lf NOT LIKE ?
GROUP BY lf
ORDER BY n DESC;
"""
SQL_1toN_F2 = """
SELECT lf, count(commitid) AS n 
FROM _change_coupling
WHERE lf != rf
AND lf NOT LIKE ?
AND lf NOT LIKE ?
GROUP BY lf
ORDER BY n DESC;
"""
SQL_1toN_F5 = """
SELECT lf, count(commitid) AS n 
FROM _change_coupling
WHERE lf != rf
AND lf NOT LIKE ?
AND lf NOT LIKE ?
AND lf NOT LIKE ?
AND lf NOT LIKE ?
AND lf NOT LIKE ?
GROUP BY lf
ORDER BY n DESC;
"""
SQL_1toN_F7 = """
SELECT lf, count(commitid) AS n 
FROM _change_coupling
WHERE lf != rf
AND lf NOT LIKE ?
AND lf NOT LIKE ?
AND lf NOT LIKE ?
AND lf NOT LIKE ?
AND lf NOT LIKE ?
AND lf NOT LIKE ?
AND lf NOT LIKE ?
GROUP BY lf
ORDER BY n DESC;
"""
SQL_1toN_ONLY = """
SELECT lf, count(commitid) AS n 
FROM _change_coupling
WHERE lf != rf
AND lf LIKE ?
GROUP BY lf
ORDER BY n DESC;
"""
SQL_1to1_NODUP = """
SELECT lf, rf, count(commitid) AS n 
FROM _change_coupling
WHERE lf < rf
GROUP BY lf, rf
ORDER BY n DESC;
"""
SQL_1to1_DUP_FILTER = """
SELECT lf, rf, count(commitid) AS n 
FROM _change_coupling
WHERE lf != rf
AND lf like ?
GROUP BY lf, rf
ORDER BY n DESC;
"""


def sql_1toN_single(view='_change_coupling'):
    return f"""
    SELECT lf, rf, count(commitid) AS n, 100.0*count(commitid) / (select count(distinct commitid) as n from {view} where lf like ?) as percent_coupling
    FROM {view}
    WHERE lf != rf
    AND lf like ?
    GROUP BY lf, rf
    ORDER BY n DESC;
    """


SQL_1to1_DUP_FILTER_NO_MERGE = '''
SELECT lf, rf, count(_change_coupling.commitid) AS n
FROM _change_coupling join Commits C on _change_coupling.commitid = C.commitid
WHERE lf != rf
AND lf like ?
AND message not like 'Merge%'
GROUP BY lf, rf
ORDER BY n DESC;
'''
SQL_MODULE_PAIR = """
SELECT *
FROM _change_coupling join Commits C on _change_coupling.commitid = C.commitid
WHERE lf like ? AND rf like ? AND message not like ?;
"""

SQL_GET_TAGS = """select Tags.tag,
       Tags.message as tagm,
       Commits.commitid,
       Commits.committerts,
       Commits.message
from Tags
         join Commits using (commitid)
where tag like ?"""  # 'v1.0%'

SQL_CTS_A = """select count(*) from _change_coupling where committerts < (select (min(committerts)+max(committerts))/2 from _change_coupling);"""
SQL_CTS_B = """select count(*) from _change_coupling where committerts >= (select (min(committerts)+max(committerts))/2 from _change_coupling);"""
