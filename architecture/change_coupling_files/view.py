from a_lib.repo_db import get_conn


def delete_change_coupling(conn):
    c = conn.cursor()
    try:
        c.execute("drop table if exists _change_coupling")
    except:
        pass
    else:
        print("drop table if exists _change_coupling --done")
    try:
        c.execute("drop view if exists _change_coupling")
    except:
        pass
    else:
        print("drop view if exists _change_coupling --done")
    conn.commit()
    c.close()


def create_change_coupling(conn):
    sql_view = """
    create view _change_coupling as
    select commitid, committerts, L.newpath as lf, R.newpath as rf
    FROM filemods AS L join filemods AS R
    using (commitid)
    join Branches using (commitid)
    join Commits using (commitid)
    where L.modtype not like 'DELETE'
    and R.modtype not like 'DELETE'
    and branch like 'refs/heads/master'
    order by committerts;
    """
    c = conn.cursor()
    try:
        c.execute("drop table if exists _change_coupling")
    except:
        pass
    try:
        c.execute("drop view if exists _change_coupling")
    except:
        pass
    c.execute(sql_view)
    conn.commit()
    c.close()


def create_view_before(name, default=True):
    if default:
        sql_view = """
        create view _change_coupling_before as
        select * from _change_coupling
        where committerts < (select (min(committerts)+max(committerts))/2 from _change_coupling);
        """
    else:
        sql_view = """
        create view _change_coupling_before as
        select * from _change_coupling
        where committerts < (
            WITH C_Master AS (
            select distinct Commits.*
            from Commits
                     join Branches using (commitid)
            where branch like 'refs/heads/master'
            )
            SELECT committerts FROM C_Master ORDER BY committerts LIMIT 1 OFFSET (SELECT COUNT(*) FROM C_Master) / 2
        );
        """
    conn = get_conn(name)
    c = conn.cursor()
    try:
        c.execute("drop table if exists _change_coupling_before")
    except:
        pass
    try:
        c.execute("drop view if exists _change_coupling_before")
    except:
        pass
    c.execute(sql_view)
    conn.commit()
    c.close()


def create_view_after(name, default=True):
    if default:
        sql_view = """
        create view _change_coupling_after as
        select * from _change_coupling
        where committerts >= (select (min(committerts)+max(committerts))/2 from _change_coupling);
        """
    else:
        sql_view = """
        create view _change_coupling_after as
        select * from _change_coupling
        where committerts >= (
            WITH C_Master AS (
            select distinct Commits.*
            from Commits
                     join Branches using (commitid)
            where branch like 'refs/heads/master'
            )
            SELECT committerts FROM C_Master ORDER BY committerts LIMIT 1 OFFSET (SELECT COUNT(*) FROM C_Master) / 2
        );
        """
    conn = get_conn(name)
    c = conn.cursor()
    try:
        c.execute("drop table if exists _change_coupling_after")
    except:
        pass
    try:
        c.execute("drop view if exists _change_coupling_after")
    except:
        pass
    c.execute(sql_view)
    conn.commit()
    c.close()
