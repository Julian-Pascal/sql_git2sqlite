from architecture.change_coupling_files.hilfsfunktionen import *

# Arsnova Cards
from architecture.change_coupling_files.view import create_view_after, create_view_before

cards_1n = get_dataframe('cards', SQL_1toN_ALL)
cards_11 = get_dataframe('cards', SQL_1to1_NODUP)
'''
zu „cardset.js“: <- Deep Nesting
- bereits bei einfacher Methode(Änderung, Komplexität) auffällig
- zweithöchste Kopplung
- (3 imports/ui/pool/pool.js 123) Kopplung sehr hoch + seltsam - Refactored!
- (4 imports/startup/client/registerhelper.js 198)
'''
cards1a = cc_file('cards', 'imports/ui/cardset/cardset.js')
cards2 = get_dataframe('cards', SQL_MODULE_PAIR,
                       ['imports/ui/cardset/cardset.js', 'imports/startup/client/registerhelper.js',
                                        ''])
cards3 = get_dataframe('cards', SQL_MODULE_PAIR,
                       ['imports/ui/cardset/cardset.js', 'imports/startup/client/registerhelper.js',
                                        'Merge%'])
cards4 = cc_pair('cards', 'imports/ui/cardset/cardset.js', 'imports/startup/client/registerhelper.js')
cards1b = cc_file('bootstrap', 'js/tests/unit/tooltip.js')
# arsnova.click
aclick_1n = get_dataframe('arsnova.click', SQL_1toN_ALL)
aclick_mod1 = aclick_1n.at[2, 'lf']
aclick1 = cc_file('arsnova.click', aclick_mod1)
aclick_11 = get_dataframe('arsnova.click', SQL_1to1_NODUP)
aclick_1 = cc_file('bootstrap', 'js/tests/unit/tooltip.js')
'''

'''

# Bootstrap
bootstrap_1n = g
bootstrap_11 = get_dataframe('bootstrap', SQL_1to1_NODUP)
# js/tests/unit/tooltip.js 9981
bootstrap_1 = cc_file('bootstrap', 'js/tests/unit/tooltip.js')
'''

'''
bootstrap_v1 = get_dataframe('bootstrap', SQL_GET_TAGS, ['v1.0%'])
bootstrap_v1_time = bootstrap_v1.at[0,'committerts']
create_view_before('bootstrap', bootstrap_v1_time)
create_view_after('bootstrap', bootstrap_v1_time)
# HugSQL
hugsql_a = get_dataframe('hugsql', SQL_1toN_ALL)
# 'hugsql-core/project.clj' hat die stärkste Kopplung
# hugsql_b = get_dataframe('hugsql', SQL_1to1_DUP_FILTER_NO_MERGE, 'hugsql-core/project.clj')
hugsql_b = cc_file('hugsql', 'hugsql-core/project.clj')
#  0  hugsql-core/project.clj  project.clj                                                                26
#  1  hugsql-core/project.clj  hugsql-adapter-clojure-java-jdbc/project.clj                               25
#  2  hugsql-core/project.clj  hugsql-adapter-clojure-jdbc/project.clj                                    24
#  3  hugsql-core/project.clj  hugsql-adapter/project.clj                                                 22
hugsql_c = get_dataframe('hugsql', SQL_MODULE_PAIR, ['hugsql-core/project.clj', 'project.clj', ''])
hugsql_d = cc_pair('hugsql', 'hugsql-core/project.clj', 'project.clj')  # 0.875
hugsql_v1 = get_dataframe('hugsql', SQL_GET_TAGS, ['%'])
'''

'''
h2database_1n = get_dataframe('h2database', SQL_1toN_ALL)
h2database_mod1 = h2database_1n.at[0, 'lf']
h2database_mod2 = h2database_1n.at[1, 'lf']
h2database_mod3 = h2database_1n.at[2, 'lf']
h2database_1 = cc_file('h2database', h2database_mod1)
h2database_2 = cc_file('h2database', h2database_mod2)
h2database_3 = cc_file('h2database', h2database_mod3)
# _v1 = get_dataframe_and_create_view('', SQL_GET_TAGS, ['%']) 🍪
hugsql_v1 = get_dataframe('hugsql', SQL_GET_TAGS, ['%']) # Keine Version über 0.4.9 ⛔
git2pg_v1 = get_dataframe('git2pg', SQL_GET_TAGS, ['%']) # Keine Versionierung ⛔
aclick_v1 = get_dataframe('arsnova.click', SQL_GET_TAGS, ['%'])  # Keine Version vor v1.5.3 ⛔
cards_v1 = get_dataframe('cards', SQL_GET_TAGS, ['%'])  # Keiner Version vor v2.1.0 ⛔
h2database_v1 = get_dataframe('h2database', SQL_GET_TAGS, ['%'])  # version-1.0 ✅
jgit_v1 = get_dataframe('jgit', SQL_GET_TAGS, ['v1%'])  # v1.0.0.x mehrere Versionen ❔
bootstrap_v1 = get_dataframe('bootstrap', SQL_GET_TAGS, ['v1%'])  # v1.0.0 ✅
atom_v1 = get_dataframe('atom', SQL_GET_TAGS, ['v1%'])  # v1.0.0 ✅
git_v1 = get_dataframe('git', SQL_GET_TAGS, ['v1%']) # v1.0.0 ✅

create_view_before('hugsql', default=False)