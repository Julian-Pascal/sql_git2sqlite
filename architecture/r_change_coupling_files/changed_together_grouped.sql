select lf, rf, count(*) as n
from (SELECT L.oldpath as lf, R.oldpath as rf
FROM filemods AS L, filemods AS R
WHERE L.commitid=R.commitid
and L.modtype like 'modify'
and R.modtype like 'modify'
and L.oldpath < R.oldpath)
group by lf, rf
order by n desc;