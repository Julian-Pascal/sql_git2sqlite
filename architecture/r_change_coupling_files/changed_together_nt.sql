--drop table _changed_together;
CREATE TABLE _changed_together AS
(SELECT L.oldpath as lf, R.oldpath as rf
FROM filemods AS L, filemods AS R
WHERE L.commitid=R.commitid
and L.modtype like 'modify'
and R.modtype like 'modify'
and L.oldpath < R.oldpath)
