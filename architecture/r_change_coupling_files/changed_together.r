library(DBI)
library(treemap)
library(ggplot2)
name <- "hugsql"
conn <- dbConnect(RSQLite::SQLite(), paste('../sqlite-files/', name, '_mirror.sqlite3',sep=""))

sql1 <- paste("SELECT L.oldpath as lf, R.oldpath as rf", 
"FROM filemods AS L join filemods AS R",
"using (commitid)",
"where L.modtype like 'modify'",
"and R.modtype like 'modify'",
"and L.oldpath < R.oldpath;")

res <- dbSendQuery(conn, sql1)
fetched <- dbFetch(res)

tab <- table(fetched)
m <- as.matrix(tab)
heatmap(m)