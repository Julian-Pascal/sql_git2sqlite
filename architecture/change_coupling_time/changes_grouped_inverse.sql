select sum(case when (newpath not like ? and oldpath not like ?) then insertcnt + replacecnt + deletecnt else 0 end )
from Commits join Filemods using (commitid)
group by round(committerts/86400000);