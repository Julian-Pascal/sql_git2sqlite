create view delta_t_tests as
select count(commitid) as '𝚫/T Tests'
from Commits join tests using (commitid)
group by round(committerts/86400000);

create view delta_t_production as
select count(commitid) as '𝚫/T Tests'
from Commits join production using (commitid)
group by round(committerts/86400000);