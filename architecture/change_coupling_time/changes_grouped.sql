select sum(case when (newpath like ? or oldpath like ?) then insertcnt + replacecnt + deletecnt else 0 end )
from Commits join Filemods using (commitid)
group by round(committerts/86400000);

select commitid, message, newpath, sum(insertcnt), sum(replacecnt)
from Commits join Filemods using (commitid)
where newpath like '%test%'
and (insertcnt + replacecnt) > 0
group by newpath
order by (insertcnt + replacecnt) desc;

--CHANGES_GROUPED = '''
select round(committerts/86400000)*86400 as xa,
 sum(case when (newpath like ? or oldpath like ?) then insertcnt + replacecnt + deletecnt else 0 end ) as ya
from Commits join Filemods using (commitid)
group by round(committerts/86400000);
--'''

--CHANGES_GROUPED_INVERSE = '''
select round(committerts/86400000)*86400 as xa,
 sum(case when (newpath not like ? and oldpath not like ?) then insertcnt + replacecnt + deletecnt else 0 end ) as ya
from Commits join Filemods using (commitid)
group by round(committerts/86400000);
--'''
select * from (
                  select round(committerts / 86400000) * 86400 as xa,
                         sum(case
                                 when (newpath like ? and newpath not like ?) then insertcnt + replacecnt
                                 else 0 end)                   as ya
                  from Commits
                           join Filemods using (commitid)
                  group by round(committerts / 86400000)
              )
where ya > 0;

-- 'arsnova.click/client/layout/global/styles/themes/theme-MetesTheme.scss'