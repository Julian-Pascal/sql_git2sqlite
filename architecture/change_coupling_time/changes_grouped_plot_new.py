import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas import DataFrame
import datetime
import seaborn as sns; sns.set()
import textwrap as tw

from a_lib.repo_db import get_conn, REPO_NAMES

CHANGES_GROUPED = """\
with C_Master AS (
    select distinct Commits.*
    from Commits
             join Branches using (commitid)
    where branch like 'refs/heads/master'
)
select strftime('%Y-%m-%d', authorts / 1000, 'unixepoch')                 as date
     , count(distinct commitid) FILTER ( WHERE newpath not like '%test%') as Anwendung
     , count(distinct commitid) FILTER ( WHERE newpath like '%test%') as Tests
from C_Master
         join Filemods using (commitid)
group by date;
"""


def production_test_coupling(name):
    df = pd.read_sql(CHANGES_GROUPED, get_conn(name))
    df['date'] = pd.to_datetime(df['date'])
    df = df.set_index('date')
    df = df.cumsum()
    df.plot(title=name)
    plt.savefig(f'output/{name}.pdf')
    plt.show()
    return df


def run_all():
    for name in REPO_NAMES:
        production_test_coupling(name)
