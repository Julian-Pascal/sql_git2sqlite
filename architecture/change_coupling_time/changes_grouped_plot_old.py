import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas import DataFrame
import datetime
import seaborn as sns; sns.set()

from a_lib.repo_db import get_conn, REPO_NAMES

CHANGES_GROUPED = '''
with C_Master AS (
select distinct Commits.* from Commits join Branches using (commitid)
where branch like 'refs/heads/master'
)
select strftime('%Y-%m-%d', authorts / 1000, 'unixepoch') as xa,
 sum(case when (newpath like ? and newpath not like ?) then insertcnt + replacecnt else 0 end ) as ya
from C_Master join Filemods using (commitid)
group by xa;
'''


def changes_grouped(name: str, val: str, not_val: str) -> pd.DataFrame:
    conn = get_conn(name)
    data = pd.read_sql(CHANGES_GROUPED, conn, params=[val, not_val])
    data['xa'] = pd.to_datetime(data['xa'])
    data['ya'] = np.cumsum(data['ya'])
    conn.close()
    return data


def production_test_coupling(name):
    prod_data: DataFrame = changes_grouped(name, '%', '%test%')
    test_data: DataFrame = changes_grouped(name, '%test%', '')
    print(type(prod_data['xa']))
    print(prod_data)
    fig, ax = plt.subplots()
    fig.autofmt_xdate()
    plt.plot(prod_data['xa'], prod_data['ya'], label='Anwendung')
    plt.plot(test_data['xa'], test_data['ya'], label='Tests')
    plt.xlabel('Datum')
    plt.ylabel('Summe der Änderungen')
    plt.legend()
    plt.title(name)
    plt.savefig(name+'_mirror.png')
    plt.show()
    plt.close()

# production_test_coupling('arsnova.click')


def run_all():
    for name in REPO_NAMES:
        production_test_coupling(name)
