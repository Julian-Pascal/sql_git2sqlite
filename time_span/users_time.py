import sqlite3
import matplotlib.pyplot as plt
import numpy as np

SQL_AUTHORS = '''
select count(distinct author) as authors
from Commits join Filemods using (commitid)
group by round(committerts/(86400000*30));
'''
SQL_COMMITTERS = '''
select count(distinct committer) as committers
from Commits join Filemods using (commitid)
group by round(committerts/(86400000*30));
'''
SQL_USERS = '''
select count(distinct user) as users
from (
    select author as user, committerts
    from Commits
    union all
    select committer as user, committerts
    from Commits
    )
group by round(committerts/(86400000*30));
'''

namelist = ['arsnova.click',
            'atom',
            'bootstrap',
            'cards',
            'git2pg',
            'git',
            'h2database',
            'hugsql',
            'jgit']

for name in namelist:
    conn = sqlite3.connect('../sqlite-files/' + name + '_mirror.sqlite3')
    c = conn.cursor()
    c.execute(SQL_AUTHORS)
    authors = c.fetchall()
    c.execute(SQL_COMMITTERS)
    committers = c.fetchall()
    c.execute(SQL_USERS)
    users = c.fetchall()
    c.close()
    conn.close()

    print(authors)
    print(committers)
    print(users)

    plt.plot(authors, label='Authors')
    plt.plot(committers, label='Committers')
    plt.plot(users, label='Users')
    plt.xlabel('aktive Monate')
    plt.ylabel('Personen')
    plt.legend()
    plt.title(name)
    plt.savefig('./users_time_out/' + name + '.pgf')
    plt.show()
