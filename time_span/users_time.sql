select count(distinct author) as authors
from Commits join Filemods using (commitid)
group by round(committerts/(86400000*30));

select count(distinct committer) as committers
from Commits join Filemods using (commitid)
group by round(committerts/(86400000*30));

select count(distinct user) as users
from (
    select author as user, committerts
    from Commits
    union all
    select committer as user, committerts
    from Commits
    )
group by round(committerts/(86400000*30));