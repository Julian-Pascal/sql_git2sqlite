import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import textwrap as tw

from a_lib.repo_db import get_conn, REPO_NAMES

sns.set()


# '%Y-%m-%d'
def get_churn(name, date_form='%Y-%m', trunc_bf=None, trunc_af=None, plot=True):
    sql = tw.dedent("""\
        WITH C_Master AS (
            select distinct Commits.*
            from Commits
                     join Branches using (commitid)
            where branch like 'refs/heads/master'
        )
        SELECT strftime(?, authorts / 1000, 'unixepoch') as date
             , sum(insertcnt + replacecnt)               as 'added'
             --, sum(deletecnt + replacecnt)               as 'deleted'
             --, count(distinct commitid)                  as commits
             , sum(insertcnt - deletecnt)                as loc
        FROM C_Master
                 join Filemods using (commitid)
        group by date
        order by date asc;
        """)
    #print(sql)

    df = pd.read_sql(sql, get_conn(name), params=(date_form,))
    df['date'] = pd.to_datetime(df['date'])
    df['loc'] = df['loc'].cumsum()
    df = df.set_index('date')
    if trunc_af or trunc_bf:
        print("trunc")
        df = df.truncate(before=trunc_bf, after=trunc_af)
    if plot:
        my_plot(df, name)
    return df


def my_plot(df, name, logy=False):
    df.plot(logy=logy)
    plt.savefig(f'output/{name}.pdf')
    plt.show()


def alt_churn():
    df = pd.read_csv('mylog')
    df['date'] = pd.to_datetime(df['date'])
    df = df.set_index('date')
    df = df.groupby(pd.Grouper(freq='M')).agg({'added': 'sum', 'deleted': 'sum', 'commits': 'sum'})
    df['loc'] = df['added'] - df['deleted']
    df['loc'] = df['loc'].cumsum()
    df.plot(logy=True)
    plt.show()
