import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

from a_lib.repo_db import get_conn, REPO_NAMES

FILES_COMMITS_TIME = """
select round(committerts/86400000)*86400 as t,
       count(distinct newpath) as 'Dateien',
       count(commitid) as 'Commits'
from Filemods join Commits using (commitid)
group by round(committerts/86400000);
"""


def files_commits_time(name):
    df = pd.read_sql(FILES_COMMITS_TIME, get_conn(name))
    df['t'] = pd.to_datetime(df['t'], unit='s')
    df[['Dateien', 'Commits']] = df[['Dateien', 'Commits']].cumsum()
    df.plot(x='t')
    print(df)
    plt.show()
    return df


# files_commits_time('cards')
