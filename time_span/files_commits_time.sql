select round(committerts/86400000)*86400 as t,
       count(distinct newpath) as 'Dateien',
       count(commitid) as 'Commits'
from Filemods join Commits using (commitid)
group by round(committerts/86400000);