select newpath, count(distinct author) as author_count, count(id) as n_revs
from Filemods
join Commits using (commitid)
join Branches B using (commitid)
where branch like '%master'
group by newpath
order by author_count desc
limit 5;