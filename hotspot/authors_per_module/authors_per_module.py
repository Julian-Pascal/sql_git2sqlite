import pandas as pd
from pandas import DataFrame
from IPython.display import display, HTML

from a_lib.repo_db import get_conn, REPO_NAMES

AUTHORS_PER_MODULE = """
select newpath, count(distinct author) as author_count, count(id) as n_revs
from Filemods
join Commits using (commitid)
group by newpath
order by author_count desc
limit 5;
"""


def write_latex(name, df: DataFrame):
    with open(name + '_apm.tex', 'w') as f:
        f.write(df.to_latex())


def write_all(names):
    for name in names:
        ds = pd.read_sql(AUTHORS_PER_MODULE, get_conn(name))
        write_latex(name, ds)


# ds = pd.read_sql(AUTHORS_PER_MODULE, get_conn('cards'))
# print(ds)
