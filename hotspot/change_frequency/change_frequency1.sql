﻿SELECT newpath,
       MAX(linecnt)   lines,
       COUNT(*) AS    changes,
       count(message) FILTER ( WHERE message like '%fix%') AS 'count of fix'
FROM filemods
         join Commits C on Filemods.commitid = C.commitid
WHERE modtype LIKE 'modify'
  AND (newpath NOT LIKE '%.json'
    AND newpath NOT LIKE '%.md'
    AND newpath NOT LIKE '%.txt'
    AND newpath NOT LIKE '%.xml'
    AND newpath NOT LIKE '%.iml'
    AND newpath NOT LIKE '%.css'
    AND newpath NOT LIKE '%.html')
GROUP BY newpath
ORDER BY lines * changes DESC
LIMIT 6;