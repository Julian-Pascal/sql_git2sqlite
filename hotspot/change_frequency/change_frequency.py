import pandas as pd
from pandas import DataFrame
import textwrap as tw

from a_lib.repo_db import get_conn, REPO_NAMES

pd.set_option('display.max_colwidth', 255)


def aclick_naiv():
    sql = """\
    WITH C_Master AS (
        select distinct Commits.*
        from Commits
                 join Branches using (commitid)
        where branch like 'refs/heads/master'
    )
    SELECT newpath,
           MAX(linecnt)                                           lines,
           COUNT(*)                                            AS revs,
           count(message) FILTER ( WHERE message like '%fix%') AS fix
    FROM filemods
             join C_Master C on Filemods.commitid = C.commitid
    WHERE modtype LIKE 'modify'
    GROUP BY newpath
    ORDER BY revs DESC, lines DESC
    LIMIT 6;
    """
    df = change_frequency('arsnova.click',sql)
    write_latex('aclick', df)


SQL_CF = f"""\
WITH C_Master AS (
    select distinct Commits.*
    from Commits
             join Branches using (commitid)
    where branch like 'refs/heads/master'
)
SELECT newpath,
       MAX(linecnt)                                           lines,
       COUNT(*)                                            AS revs,
       count(message) FILTER ( WHERE message like '%fix%') AS fix
FROM filemods
         join C_Master C on Filemods.commitid = C.commitid
WHERE modtype LIKE 'modify'
  AND (newpath NOT LIKE '%.json'
    AND newpath NOT LIKE '%.md'
    AND newpath NOT LIKE '%.txt'
    AND newpath NOT LIKE '%.xml'
    AND newpath NOT LIKE '%.iml'
    AND newpath NOT LIKE '%.css'
    AND newpath NOT LIKE '%.html'
    AND newpath NOT LIKE '%.csv')
GROUP BY newpath
ORDER BY revs DESC, lines DESC
LIMIT 6;
"""

def change_frequency(name, sql) -> pd.DataFrame:
    conn = get_conn(name)
    res = pd.read_sql(sql, conn)
    # f = open(name+'.tex', 'w')
    return res


def create_table(df: DataFrame, name):
    conn = get_conn(name)
    df.to_sql("_change_frequency", conn)


def do_all():
    for name in REPO_NAMES:
        df: DataFrame = change_frequency(name, SQL_CF)
        write_latex(name, df)


def create_all():
    for name in REPO_NAMES:
        df: DataFrame = change_frequency(name)
        create_table(df, name)


# Repo-Spezifische Funktionen
# https://github.com/twbs/bootstrap/commit/869bdff100dd860d31d6a33f1367caa4df2e03c1#diff-2757cd21af75a7f198f845bbd0a1a748
def bootstrap_cf():
    #     AND newpath NOT LIKE 'dist/%'
    #     AND newpath NOT LIKE 'docs/%'
    bootstrap_sql = tw.dedent("""\
    WITH C_Master AS (
        select distinct Commits.*
        from Commits
                 join Branches using (commitid)
        where branch like 'refs/heads/master'
    )
    SELECT newpath,
           MAX(linecnt)                                           lines,
           COUNT(*)                                            AS revs,
           count(message) FILTER ( WHERE message like '%fix%') AS fix
    FROM filemods
             join C_Master C on Filemods.commitid = C.commitid
    WHERE modtype LIKE 'modify'
      AND (newpath NOT LIKE '%.json'
        AND newpath NOT LIKE '%.md'
        AND newpath NOT LIKE '%.txt'
        AND newpath NOT LIKE '%.xml'
        AND newpath NOT LIKE '%.iml'
        AND newpath NOT LIKE '%.css'
        AND newpath NOT LIKE '%.html'
        AND newpath NOT LIKE '%.csv'
        --
        AND newpath NOT LIKE 'dist/%'
        AND newpath NOT LIKE 'docs/%')
    GROUP BY newpath
    ORDER BY revs DESC, lines DESC
    LIMIT 6;
    """)
    repo_name = 'bootstrap'
    df = change_frequency(repo_name, bootstrap_sql)
    write_latex(repo_name, df)
    return df


def jgit_cf():
    #     AND newpath NOT LIKE 'dist/%'
    #     AND newpath NOT LIKE 'docs/%'
    jgit_sql = tw.dedent("""\
    WITH C_Master AS (
        select distinct Commits.*
        from Commits
                 join Branches using (commitid)
        where branch like 'refs/heads/master'
    )
    SELECT newpath,
           MAX(linecnt)                                           lines,
           COUNT(*)                                            AS revs,
           count(message) FILTER ( WHERE message like '%fix%') AS fix
    FROM filemods
             join C_Master C on Filemods.commitid = C.commitid
    WHERE modtype LIKE 'modify'
      AND (newpath NOT LIKE '%.json'
        AND newpath NOT LIKE '%.md'
        AND newpath NOT LIKE '%.txt'
        AND newpath NOT LIKE '%.xml'
        AND newpath NOT LIKE '%.iml'
        AND newpath NOT LIKE '%.css'
        AND newpath NOT LIKE '%.html'
        AND newpath NOT LIKE '%.csv'
        --
        AND newpath NOT LIKE '%.MF')
    GROUP BY newpath
    ORDER BY revs DESC, lines DESC
    LIMIT 6;
    """)
    repo_name = 'jgit'
    df = change_frequency(repo_name, jgit_sql)
    write_latex(repo_name, df)
    return df


def write_latex(name, df: DataFrame):
    with open(f'output/{name}.tex', 'w') as f:
        f.write(df.to_latex())

def do_all1():
    do_all()
    bootstrap_cf()
    jgit_cf()

# print(change_frequency('arsnova.click'))

# do_all()

# print(change_frequency('git2pg'))