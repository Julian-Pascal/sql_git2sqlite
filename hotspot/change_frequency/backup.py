SQL_MASTER = """SELECT newpath,
       MAX(linecnt)   lines,
       COUNT(*) AS    changes,
       count(message) FILTER ( WHERE message like '%fix%') AS 'count of fix'
FROM filemods
         join Commits C on Filemods.commitid = C.commitid
         join Branches B on C.commitid = B.commitid
WHERE branch like 'refs/heads/master'
  AND modtype LIKE 'modify'
  AND (newpath NOT LIKE '%.json'
    AND newpath NOT LIKE '%.md'
    AND newpath NOT LIKE '%.txt'
    AND newpath NOT LIKE '%.xml'
    AND newpath NOT LIKE '%.iml'
    AND newpath NOT LIKE '%.css'
    AND newpath NOT LIKE '%.html')
GROUP BY newpath
ORDER BY lines * changes DESC
LIMIT 6;
    """

SQL_FILTER = """AND (newpath NOT LIKE '%.json'
    AND newpath NOT LIKE '%.md'
    AND newpath NOT LIKE '%.txt'
    AND newpath NOT LIKE '%.xml'
    AND newpath NOT LIKE '%.iml'
    AND newpath NOT LIKE '%.css'
    AND newpath NOT LIKE '%.html')"""
SQL1_MASTER = f"""SELECT newpath,
       MAX(linecnt)   lines,
       COUNT(*) AS    changes,
       count(message) FILTER ( WHERE message like '%fix%') AS 'count of fix'
FROM filemods
         join Commits C on Filemods.commitid = C.commitid
         join Branches B on C.commitid = B.commitid
WHERE branch like 'refs/heads/master'
  AND modtype LIKE 'modify'
  {SQL_FILTER}
GROUP BY newpath
ORDER BY changes DESC, lines DESC
LIMIT 6;
"""

def h2database_cf():
    # AND newpath NOT LIKE '%.csv'
    h2_sql = tw.dedent("""\
    SELECT newpath,
       MAX(linecnt)                                           lines,
       COUNT(*)                                            AS changes,
       count(message) FILTER ( WHERE message like '%fix%') AS 'count of fix'
FROM filemods
         join Commits C on Filemods.commitid = C.commitid
         join Branches B on C.commitid = B.commitid
WHERE branch like 'refs/heads/master'
  AND modtype LIKE 'modify'
  AND (newpath NOT LIKE '%.json'
    AND newpath NOT LIKE '%.md'
    AND newpath NOT LIKE '%.txt'
    AND newpath NOT LIKE '%.xml'
    AND newpath NOT LIKE '%.iml'
    AND newpath NOT LIKE '%.css'
    AND newpath NOT LIKE '%.html'
    --
    AND newpath NOT LIKE '%.csv')
GROUP BY newpath
ORDER BY lines * changes DESC
LIMIT 5;
    """)
    repo_name = 'h2database'
    df = change_frequency(repo_name, h2_sql)
    write_latex(repo_name, df)
    return df