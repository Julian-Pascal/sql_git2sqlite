library(DBI)
library(treemap)
name <- "git2pg"
conn <- dbConnect(RSQLite::SQLite(), paste('../sqlite-files/', name, '_mirror.sqlite3',sep=""))
sql <- "select max(linecnt) as lines,oldpath from filemods where modtype like 'modify' group by oldpath order by lines desc;"
res <- dbSendQuery(conn, sql)
fetched <- head(dbFetch(res),100)
treemap(fetched, index="oldpath", vSize="lines")